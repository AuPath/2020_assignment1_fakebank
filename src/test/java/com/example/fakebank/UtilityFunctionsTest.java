package com.example.fakebank;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UtilityFunctionsTest {

    @Test
    void convertBalanceToFakeCurrency() {
        UtilityFunctions utilityFunctions = new UtilityFunctions();
        float convertedBalance = utilityFunctions.convertBalanceToFakeCurrency(20, 0.5f);
        assertEquals(10, convertedBalance);
    }
}