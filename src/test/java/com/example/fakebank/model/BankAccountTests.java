package com.example.fakebank.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


import static org.junit.jupiter.api.Assertions.assertEquals;

public class BankAccountTests {

    @Test
    public void bankAccountNameValidation() {
        String firstName = "    GianlUca     ";
        String lastName = "    GiuDiCE        ";
        BankAccount bankAccount = new BankAccount("999", firstName, lastName, 1234);
        assertEquals(bankAccount.getFirstName(), "GIANLUCA");
        assertEquals(bankAccount.getLastName(), "GIUDICE");
    }

}
