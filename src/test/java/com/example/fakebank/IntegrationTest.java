package com.example.fakebank;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.example.fakebank.model.BankAccount;
import com.example.fakebank.repository.BankAccountRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@SpringBootTest(classes = {FakebankApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class IntegrationTest {

    @Autowired
    private MockMvc mvc;

    private final String endpoint = "/api/accounts/balance/";
    private final static int id = 123;

    @BeforeAll
    public static void addUsers(@Autowired BankAccountRepository repository){
        repository.deleteAll();
        repository.save(new BankAccount(String.valueOf(id), "        Gianluca", "Giudice", 1450));
        repository.save(new BankAccount(String.valueOf(id + 1), "Macro     ", "Grassi", 500));
    }

    @Test
    public void getBalance() throws Exception {
        mvc.perform(get(endpoint + id).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void deposit() throws Exception {
        // Amount to deposit
        float depositAmount = 100;
        float balance = getAmountById(id);
        // Deposit
        mvc.perform(post(endpoint + id + "/deposit/" + depositAmount));
        // New amount
        float newBalance = getAmountById(id);
        // Assertion
        assertEquals(newBalance, balance + depositAmount);
    }

    @Test
    public void withdraw() throws Exception {
        // Amount to deposit
        float withdrawAmount = 500;
        float balance = getAmountById(id);
        // Deposit
        mvc.perform(post(endpoint + id + "/withdraw/" + withdrawAmount));
        // New amount
        float newBalance = getAmountById(id);
        // Assertion
        assertEquals(newBalance, balance - withdrawAmount);
    }

    @Test
    public void withdrawMoreMoneyThanBalance() throws Exception {
        // Amount to deposit
        float balance = getAmountById(id);
        float withdrawAmount = balance + 1;
        // Deposit
        mvc.perform(post(endpoint + id + "/withdraw/" + withdrawAmount));
        // New amount
        float newBalance = getAmountById(id);
        // Assertion
        assertEquals(newBalance, balance);
    }

    private float getAmountById(int id) throws Exception {
        MvcResult result = mvc.perform(get(endpoint + id).contentType(MediaType.APPLICATION_JSON))
                .andReturn();
        return Float.parseFloat(result.getResponse().getContentAsString());
    }

}
