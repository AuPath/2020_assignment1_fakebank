package com.example.fakebank.repository;
import com.example.fakebank.model.BankAccount;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BankAccountRepository extends MongoRepository <BankAccount, String> {

    BankAccount getBankAccountById(String id);

    List<BankAccount> getBankAccountByBalanceGreaterThanEqual(float balance);

}
