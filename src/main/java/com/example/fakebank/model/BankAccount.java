package com.example.fakebank.model;

import org.springframework.data.annotation.Id;

public class BankAccount {

    @Id
    private String id;
    private String firstName;
    private String lastName;
    private float balance;

    public BankAccount(String id, String firstName, String lastName, float balance) {
        this.id = id;
        this.firstName = this.validateName(firstName);
        this.lastName = this.validateName(lastName);
        this.balance = balance;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", balance=" + balance +
                '}';
    }

    private String validateName(String name){
        return name.strip().toUpperCase();
    }
}
