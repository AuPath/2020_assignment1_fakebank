package com.example.fakebank.controller;

import com.example.fakebank.UtilityFunctions;
import com.example.fakebank.model.BankAccount;
import com.example.fakebank.repository.BankAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/accounts")
public class FakeBankController {

    private BankAccountRepository bankAccountRepository;
    private UtilityFunctions utilityFunctions;

    @Autowired
    public FakeBankController(BankAccountRepository bankAccountRepository, UtilityFunctions utilityFunctions) {
        this.bankAccountRepository = bankAccountRepository;
        this.utilityFunctions = utilityFunctions;
    }

    @GetMapping("/balance/{id}")
    public float getBankAccountBalance(@PathVariable("id") String id)
    {
        return bankAccountRepository.getBankAccountById(id).getBalance();
    }

    @GetMapping("/balance/{id}/{factor}")
    public float getBankAccountBalanceConvertedBy(@PathVariable("id") String id, @PathVariable("factor") float conversionFactor)
    {
        return utilityFunctions.convertBalanceToFakeCurrency(bankAccountRepository.getBankAccountById(id).getBalance(), conversionFactor);
    }

    @PostMapping("/balance/{id}/deposit/{amount}")
    public float deposit(@PathVariable("id") String id, @PathVariable("amount") float amount){
        // Get target bankAccount
        BankAccount target = bankAccountRepository.getBankAccountById(id);
        // Update balance
        target.setBalance(target.getBalance() + amount);
        // Save to database
        bankAccountRepository.save(target);
        // Return new balance
        return target.getBalance();
    }

    @PostMapping("/balance/{id}/withdraw/{amount}")
    public float withdraw(@PathVariable("id") String id, @PathVariable("amount") float amount){
        // Get target bankAccount
        BankAccount target = bankAccountRepository.getBankAccountById(id);
        // Check availability of amount
        if (target.getBalance() - amount < 0){
            return -1;
        } else {
            target.setBalance(target.getBalance() - amount);
            bankAccountRepository.save(target);
            return target.getBalance();
        }
    }

}
