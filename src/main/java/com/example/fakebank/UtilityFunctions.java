package com.example.fakebank;

import org.springframework.stereotype.Component;

@Component
public class UtilityFunctions {

    public float convertBalanceToFakeCurrency(float balance, float conversionFactor)
    {
        return balance * conversionFactor;
    }
}
