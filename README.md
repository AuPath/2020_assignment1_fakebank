# Assignment 1 - Processo e Sviluppo del Software
## FakeBank

### Team members
- Gianluca Giudice - **830694**
- Marco Grassi - **829664**

### Gitlab repository
https://gitlab.com/AuPath/2020_assignment1_fakebank

### The app
FakeBank is a simple API application written in Java using the
SpringBoot Framework that uses a MongoDB database. The system can
store several users with their associated bank account. Each user can
deposit and withdraw money using the API endpoints exposed by the
backend server, which stores each transaction on the MongoDB database.

#### Usage
There are only 2 active users in the bank with the associated ID `123` and `456`.

**Base URL**: https://fakebank-production.herokuapp.com/api/

| Request type | Endpoint                                     | Description                                |
|:------------:|:---------------------------------------------|--------------------------------------------|
| `GET`        | **/accounts/balance/{id}**                   | Get the bank account balance given the ID  |
| `POST`       | **/accounts/balance/{id}/deposit/{amount}**  | Deposit some amount of money given the ID  |
| `POST`       | **/accounts/balance/{id}/withdraw/{amount}** | Withdraw some amount of money given the ID |

### Pipeline
The pipeline for this project's continuous integration / continuous
deployment implements caching mechanisms to keep running times low.

Because our project is based on Java we used Maven to manage most of
our software's lifecycle.

1. **Build**

    Download dependencies and compile the source code. Compiling the
    code is the first step in continuing through the rest of the
    pipeline.

2. **Verify**

    Maven performs code style check and static analysis. Because of
    how the pipeline was developed, both the `code-style` and the
    `static-analysis` steps refers to this stage. These steps are
    executed in parallel, resulting in an overall reduction in
    execution time.

    Custom code style specifications are stored in the
    `checkstyle.xml` file, in order to not have a too strict code
    style policy.

    When this step of the pipeline fails the artifacts are saved for
    analysis and future code corrections.


3. **Unit Test**

    The JUnit framework is used to run unit tests. Tests are stored in
    the `src/tests` folder, which contains both unit and integration
    tests. However, it is important to note that these tests are
    performed separately in two different stages.
    
    Two different unit-tests were written:
    - **BankAccountTests**: Aims to test the BankAccount model
    - **UtilityFunctionsTests**: Aims to test some utility functions


4. **Integration Test**

    The JUnit framework is used to run integration tests. This means
    executing the class `IntegrationTest` stored in the `src/tests/`
    folder.
    
    The database on which the integration tests are run is **not** the
    production database, they are run on a throwaway MongoDB database
    inside a Docker image made available by a GitLab CI service.  To
    achieve this in the `gitlab-ci.yml` file we use the MongoDB
    service with the alias `mongodb`:
   
    ```
     services:
       - name: mongo:3.7
         alias: mongodb
    ``` 

    Using this alias, we create a different `application-test.yml`
    application configuration file and then use the
    `@ActiveProfiles("test")` annotation in the `IntegrationTest.java`
    file in test folder. By combining these features of both GitLab CI
    and SpringBoot the integration test step never touches the
    production database.
   
    The integrations tests are intended to test the deposit and
    withdraw operations using the API. These operations must change the
    user's bank account balance in the database. For this purpose
    deposit and withdraw actions are performed, so the bank account
    balance should increase or decrease as a result of the action
    performed.

5. **Package**

    Maven creates an executable `.jar` file that contains each of the
    compiled classes. Test files are not included in the package.  A
    package `.jar` artifact is produced only when this pipeline step
    is successfully run.

6. **Release**

    In this stage, a docker image based on `Dockerfile` is created and
    then pushed to the GitLab container registry. The most important information contained in the Docker file is:
    ```
    EXPOSE 8080
    ```
    This is crucial as SpringBoot uses port 8080 (by default) to listen for requests.
    
    The image is cached and used in subsequent pipeline executions.

7. **Deploy**

    This is the last stage. The application is ready to be
    distributed. For this purpose Heroku, a platform as a service, is
    used to deploy the application. The result is a usable application
    hosted on the cloud.
    
    We opted to deploy our MongoDB database on [MongoDB
    Atlas](https://www.mongodb.com/cloud/atlas) instead of using a
    [Heroku MongoDB
    plugin](https://devcenter.heroku.com/changelog-items/1823) is because
    it was going to be discontinued on november 10. Other alternatives
    were also not free.
    
    The Heroku endpoint where our application will be hosted is
    managed by a secret Gitlab environment variable.
